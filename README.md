# GitKraken issue sample #

This repo serves as a sample to reproduce an issue with .gitignore in GitKraken.

## The .gitignore file ##

```
# User macros
src/macros/**
!src/macros/README.md
!src/macros/def
!src/macros/def/**
!src/macros/demo
!src/macros/demo/**
!src/macros/u
!src/macros/u/README.md
```

This file first tells git to ignore everything into `src/macros` and then sets a few exceptions:

* `README.md` inside `src/macros`
* `src/macros/def` and everything inside
* `src/macros/demo` and everything inside
* `src/macros/u`: In this folder we just want `README.md`

## The issue ##

At this point, if you create a file in `src/macros/u` and do a `git status` you'll get:
```
$ git status
On branch master
nothing to commit, working directory clean
```

... but if you go to GitKraken you'll find the new file under `Unstaged files`